"""Conan configuration for Catch2 library.

This file is based on conanfile.py in https://github.com/catchorg/Catch2.
"""

from conans import ConanFile, CMake, tools


class Catch2Conan(ConanFile):
    name = "catch2"
    version = "3.0.1"
    description = "A modern, C++-native, framework for unit-tests, TDD and BDD"
    topics = ("conan", "catch2", "unit-test", "tdd", "bdd")
    url = "https://github.com/catchorg/Catch2"
    homepage = url
    license = "BSL-1.0"

    settings = "os", "compiler", "build_type", "arch"
    options = {}
    default_options = {}

    generators = "cmake"

    def source(self):
        self.run("git clone https://github.com/catchorg/Catch2.git -b v3.0.1")
        tools.replace_in_file(
            "Catch2/CMakeLists.txt",
            'list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/CMake")',
            """list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/CMake")
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()""",
        )

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_TESTING"] = "OFF"
        cmake.definitions["CATCH_INSTALL_DOCS"] = "OFF"
        cmake.definitions["CATCH_INSTALL_HELPERS"] = "ON"
        cmake.configure(source_folder="Catch2")
        cmake.build()

    def package(self):
        self.copy(pattern="LICENSE.txt", dst="licenses", keep_path=False)
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.names["cmake_find_package"] = "Catch2"
        self.cpp_info.names["cmake_find_package_multi"] = "Catch2"

        postfix = "d" if self.settings.build_type == "Debug" else ""

        # Catch2
        self.cpp_info.components["catch2base"].names["cmake_find_package"] = "Catch2"
        self.cpp_info.components["catch2base"].names[
            "cmake_find_package_multi"
        ] = "Catch2"
        self.cpp_info.components["catch2base"].names["pkg_config"] = "Catch2"
        self.cpp_info.components["catch2base"].libs = ["Catch2" + postfix]

        # Catch2WithMain
        self.cpp_info.components["catch2main"].names[
            "cmake_find_package"
        ] = "Catch2WithMain"
        self.cpp_info.components["catch2main"].names[
            "cmake_find_package_multi"
        ] = "Catch2WithMain"
        self.cpp_info.components["catch2main"].names["pkg_config"] = "Catch2WithMain"
        self.cpp_info.components["catch2main"].libs = ["Catch2Main" + postfix]
        self.cpp_info.components["catch2main"].requires = ["catch2base"]
