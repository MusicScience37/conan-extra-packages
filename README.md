# conan-extra-packages

[![pipeline status](https://gitlab.com/MusicScience37/conan-extra-packages/badges/main/pipeline.svg)](https://gitlab.com/MusicScience37/conan-extra-packages/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Conan packages which are not in Conan Center now, but I want to use.

## Packages

| Library                                           | Version         | Package                                                   | Directory |
| :------------------------------------------------ | :-------------- | :-------------------------------------------------------- | :-------- |
| [Catch2](https://github.com/catchorg/Catch2)      | v3.0.0-preview3 | `catch2/3.0.0@MusicScience37+conan-extra-packages/stable` | Catch2_3  |
| [Celero](https://github.com/DigitalInBlue/Celero) | v2.8.2          | `celero/2.8.2@MusicScience37+conan-extra-packages/stable` | Celero    |
