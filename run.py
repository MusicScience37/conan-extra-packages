#!/usr/bin/env python3
"""Script to create and upload Conan packages.
"""

from typing import Optional, List

import subprocess
import click


ALL_PACKAGES = [
    "catch2",
]


PACKAGES = {
    "catch2": {
        "version": "3.0.1",
        "directory": "Catch2_3",
    },
}

CONAN_USER = "MusicScience37+conan-extra-packages"


def call_command(command: List[str]):
    subprocess.run(command, check=True)


@click.group()
def cli():
    pass


@click.command()
@click.argument("channel")
@click.option("-p", "--package", "package", default=None, help="Package name.")
def create(channel: str, package: Optional[str]):
    packages = ALL_PACKAGES
    if package:
        click.echo(f"create only {package}")
        packages = [package]

    for package in packages:
        for build_type in ["Debug", "Release"]:
            click.echo(f"create {package} ({build_type})")
            call_command(
                [
                    "conan",
                    "create",
                    "--build",
                    "missing",
                    "-s",
                    f"build_type={build_type}",
                    PACKAGES[package]["directory"],
                    CONAN_USER + "/" + channel,
                ]
            )

    click.echo(f"completed all {len(packages)} packages")


@click.command()
@click.argument("channel")
@click.option("-p", "--package", default=None, help="Package name.")
@click.option("-r", "--remote", default="gitlab", help="Remote server of Conan.")
def upload(channel: str, package: Optional[str], remote: str):
    packages = ALL_PACKAGES
    if package:
        click.echo(f"create only {package}")
        packages = [package]

    for package in packages:
        click.echo(f"create {package}")
        call_command(
            [
                "conan",
                "upload",
                "-r",
                remote,
                f"{package}/{PACKAGES[package]['version']}@{CONAN_USER}/{channel}",
                "--all",
            ]
        )

    click.echo(f"completed all {len(packages)} packages")


cli.add_command(create)
cli.add_command(upload)


def main():
    cli()


if __name__ == "__main__":
    main()
